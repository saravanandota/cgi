<?php

namespace Drupal\cgi_related_media\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'CGI: configurable json blog feed' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "cgi_blog_feed_block",
 *   admin_label = @Translation("CGI Blog Feed Block")
 * )
 */
class CGIBlogFeedBlock extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * This method sets the block default configuration. This configuration
   * determines the block's behavior when a block is initially placed in a
   * region. Default values for the block configuration form should be added to
   * the configuration array. System default configurations are assembled in
   * BlockBase::__construct() e.g. cache setting and block title visibility.
   *
   * @see \Drupal\block\BlockBase::__construct()
   */
  public function defaultConfiguration() {
    return [
        'blog_feed_file' => $this->t('blog_feed.json'),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * This method defines form elements for custom block configuration. Standard
   * block configuration fields are added by BlockBase::buildConfigurationForm()
   * (block title and title visibility) and BlockFormController::form() (block
   * visibility settings).
   *
   * @see \Drupal\block\BlockBase::buildConfigurationForm()
   * @see \Drupal\block\BlockFormController::form()
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['blog_feed_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Blog Feed File'),
      '#description' => $this->t('This field used to configure blog feed file.'),
      '#default_value' => $this->configuration['blog_feed_file'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * This method processes the blockForm() form fields when the block
   * configuration form is submitted.
   *
   * The blockValidate() method can be used to validate the form submission.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['blog_feed_file']
      = $form_state->getValue('blog_feed_file');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
      /** @var \GuzzleHttp\Client $client */
      $client = \Drupal::service('http_client_factory')->fromOptions([
          'base_uri' => \Drupal::request()->getSchemeAndHttpHost(),
      ]);
      $response = $client->get('blog_feed.json');
      $blogs = Json::decode($response->getBody());
      $renderable = [
          '#theme' => 'blog_feed_template',
          '#blogs' => $blogs['blogs'],
      ];
      return $renderable;
  }
  /*Block cache disabled during development*/
  public function getCacheMaxAge() {
      return 0;
  }
}