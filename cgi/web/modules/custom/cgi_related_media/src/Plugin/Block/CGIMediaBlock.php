<?php

namespace Drupal\cgi_related_media\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'CGI: media left side' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "cgi_media_block",
 *   admin_label = @Translation("CGI Media Block")
 * )
 */
class CGIMediaBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
      $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'cgi_media')
      ->sort('created'); 
      $nids = $query->execute();
      $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);
      $featured_media_announcment_arr = $featured_brochure_arr = $featured_white_paper_arr = $remaining_cgi_media_arr = array();
      $category = '';
      $i=0;
      foreach ($nodes as $node){
              $category = $node->get('field_category')->getString();
              
              if(!empty($category)){
                $category_arr = explode(',', $category);
              }
              if(in_array('Media announcement', $category_arr) && empty($featured_media_announcment_arr)){
                  $featured_media_announcment_arr = array(
                      'title' => $node->getTitle(),
                      'category_arr' => $category_arr,
                      'category' => $category,
                      'body' => $node->body->getValue()[0]['value']
                  );
                  $fid = $node->get('field_cgi_media_image')->getValue()[0]['target_id'];
                  if(isset($fid) && is_numeric($fid)){
                      $file = \Drupal\file\Entity\File::load($fid);
                      $cgi_large_image_url = \Drupal\image\Entity\ImageStyle::load('cgi_large_image')->buildUrl($file->getFileUri());
                      $featured_media_announcment_arr['cgi_large_image'] =  $cgi_large_image_url;
                  }
              }elseif(in_array('Brochure', $category_arr) && empty($featured_brochure_arr)){
                  $featured_brochure_arr = array(
                      'title' => $node->getTitle(),
                      'category' => $category,
                      'category_arr' => $category_arr,
                      'body' => $node->body->getValue()[0]['value']
                  );
                  $fid = $node->get('field_cgi_media_image')->getValue()[0]['target_id'];
                  if(isset($fid) && is_numeric($fid)){
                      $file = \Drupal\file\Entity\File::load($fid);
                      $cgi_large_image_url = \Drupal\image\Entity\ImageStyle::load('cgi_large_image')->buildUrl($file->getFileUri());
                      $featured_brochure_arr['cgi_large_image'] = $cgi_large_image_url;
                  }
              }elseif(in_array('White paper', $category_arr) && empty($featured_white_paper_arr)){
                  $featured_white_paper_arr = array(
                      'title' => $node->getTitle(),
                      'category' => $category,
                      'category_arr' => $category_arr,
                      'body' => $node->body->getValue()[0]['value']
                  );
                  $fid = $node->get('field_cgi_media_image')->getValue()[0]['target_id'];
                  if(isset($fid) && is_numeric($fid)){
                      $file = \Drupal\file\Entity\File::load($fid);
                      $cgi_thumbnail_image_url = \Drupal\image\Entity\ImageStyle::load('cgi_thumbnail_image')->buildUrl($file->getFileUri());
                      $featured_white_paper_arr['cgi_thumbnail_image'] =  $cgi_thumbnail_image_url;
                  }
              }else{
                  $remaining_cgi_media_arr[$i] = array(
                      'title' => $node->getTitle(),
                      'category' => $category,
                      'category_arr' => $category_arr,
                      'body' => $node->body->getValue()[0]['value']
                  );
                  $fid = $node->get('field_cgi_media_image')->getValue()[0]['target_id'];
                  if(isset($fid) && is_numeric($fid)){
                      $file = \Drupal\file\Entity\File::load($fid);
                      $cgi_thumbnail_image_url = \Drupal\image\Entity\ImageStyle::load('cgi_thumbnail_image')->buildUrl($file->getFileUri());
                      $remaining_cgi_media_arr[$i]['cgi_thumbnail_image'] =  $cgi_thumbnail_image_url;
                  }
                  $i ++;
              }
      }
      $renderable = [
          '#theme' => 'cgi_media_block_template',
          '#featured_media_announcment_arr' => $featured_media_announcment_arr,
          '#featured_brochure_arr' => $featured_brochure_arr,
          '#featured_white_paper_arr' => $featured_white_paper_arr,
          '#remaining_cgi_media_arr' => $remaining_cgi_media_arr,
      ];
      return $renderable;
  }
  /*Block cache disabled during development*/
  public function getCacheMaxAge() {
      return 0;
  }
}